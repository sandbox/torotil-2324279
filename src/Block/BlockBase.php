<?php

namespace Drupal\oopal\Block;

abstract class BlockBase implements BlockPluginInterface {
  public function label() {
    return '<none>';
  }
  public function access($user) {
    return TRUE;
  }
  public function build() {
    return array();
  }
  public function blockForm($form, &$form_state) {
    return $form;
  }
  public function blockValidate($form, &$form_state) {
  }
  public function blockSubmit($form, &$form_state) {
  }
  public function defaultSettings() {
    return array();
  }
  public function cachePolicy() {
    return DRUPAL_CACHE_PER_ROLE;
  }
  public function configure() {
    return array();
  }
  public function save($edit = array()) {
  }
}
