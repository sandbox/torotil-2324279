<?php
/**
 * @file
 * Contains \Drupal\oopal\Block\BlockPluginInterface.
 */
namespace Drupal\oopal\Block;
/*
 * Defines the required interface for all block plugins.
 *
 * @ingroup block_api
 */
interface BlockPluginInterface {
  /**
   * Returns the user-facing block label.
   *
   * @todo Provide other specific label-related methods in
   *   https://drupal.org/node/2025649.
   *
   * @return string
   *   The block label. Use "<none>" if you don't want a label for this block.
   */
  public function label();
  /**
   * Indicates whether the block should be shown.
   *
   * This method allows base implementations to add general access restrictions
   * that should apply to all extending block plugins.
   *
   * @param stdclass $user
   *   The user for which to check access.
   *
   * @return bool
   *   TRUE if the block should be shown, or FALSE otherwise.
   */
  public function access($user);
  /**
   * Builds and returns the renderable array for this block plugin.
   *
   * @return array
   *   A renderable array representing the content of the block.
   *
   * @see \Drupal\block\BlockViewBuilder
   */
  public function build();
  /**
   * Returns the configuration form elements specific to this block plugin.
   *
   * Blocks that need to add form elements to the normal block configuration
   * form should implement this method.
   *
   * @param array $form
   *   The form definition array for the block configuration form.
   * @param array $form_state
   *   The current state of the form.
   *
   * @return array $form
   *   The renderable form array representing the entire configuration form.
   */
  public function blockForm($form, &$form_state);
  /**
   * Adds block type-specific validation for the block form.
   *
   * Note that this method takes the form structure and form state for the full
   * block configuration form as arguments, not just the elements defined in
   * BlockPluginInterface::blockForm().
   *
   * @param array $form
   *   The form definition array for the full block configuration form.
   * @param array $form_state
   *   The current state of the form.
   *
   * @see \Drupal\block\BlockPluginInterface::blockForm()
   * @see \Drupal\block\BlockPluginInterface::blockSubmit()
   */
  public function blockValidate($form, &$form_state);
  /**
   * Adds block type-specific submission handling for the block form.
   *
   * Note that this method takes the form structure and form state for the full
   * block configuration form as arguments, not just the elements defined in
   * BlockPluginInterface::blockForm().
   *
   * @param array $form
   *   The form definition array for the full block configuration form.
   * @param array $form_state
   *   The current state of the form.
   *
   * @see \Drupal\oopal\Block\BlockPluginInterface::blockForm()
   * @see \Drupal\oopal\Block\BlockPluginInterface::blockValidate()
   */
  public function blockSubmit($form, &$form_state);

  // --------- The methods below this line are D7-only ---------
  /**
   * Get an label to be displayed on the admin interface.
   *
   * @return string
   *   Translated label.
   */
  public function adminLabel();
  /**
   * Get an array of default settings for this block.
   *
   * @return array
   *   keyed array of default configurations.
   *
   * @see hook_block_info() for a more detailed description of possible keys.
   */
  public function defaultSettings();
  /**
   * A bitmask describing what kind of caching is appropriate for the block.
   *
   * @return integer
   *   A bitmask describing the cache-policy.
   *
   * @see hook_block_info().
   */
  public function cachePolicy();
  /**
   * Configure form.
   *
   * @return array
   *   A renderable form array that's put inside $form['settings']
   */
  public function configure();
  /**
   * Save settings.
   *
   * @param array
   *   Array of submitted form values corresponding to the configure form.
   */
  public function save($edit = array());
}
