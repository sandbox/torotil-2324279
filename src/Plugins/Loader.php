<?php

namespace Drupal\oopal\Plugins;

class Loader {
  protected static $instance = NULL;
  protected $typeInfo;
  protected $pluginInfo;
  protected $cache = array();
  public function __construct() {
    $this->typeInfo = static::typeInfo();
    $this->pluginInfo = static::pluginInfo($this->typeInfo);
  }

  public static function instance() {
    if (!self::$instance) {
      self::$instance = new static();
    }
    return self::$instance;
  }

  protected static function typeInfo() {
    if ($cache = cache_get('oopal-plugin_types')) {
      $types = $cache->data;
    }
    else {
      $types = module_invoke_all('oopal_plugin_type_info');
      drupal_alter('oopal_plugin_type_info', $types);
      cache_set('oopal-plugin-types', $types);
    }
    return $types;
  }

  protected static function pluginInfo($types) {
    if ($cache = cache_get('oopal-plugin-info')) {
      $plugins = $cache->data;
    }
    else {
      $plugins = array();
      foreach ($types as $name => $interface) {
        $plugins[$name] = array();
      }
      $info = module_invoke_all('oopal_plugin_info');
      foreach ($info as $infoType => $infoPlugins) {
        if (!isset($types[$infoType])) {
          continue;
        }
        $interface = $types[$infoType];
        foreach ($info[$infoType] as $name => $plugin) {
          if (isset($plugin['class']) && is_a($plugin['class'], $interface, TRUE)) {
            $plugins[$infoType][$name] = $plugin + array('args' => array());
          }
        }
      }
      drupal_alter('oopal_plugins_info', $plugins);
      cache_set('oopal-plugin-info', $plugins);
    }
    return $plugins;
  }

  public function load($type, $name, $info = FALSE) {
    if (isset($this->cache[$type]) && isset($this->cache[$type][$name])) {
      return $this->cache[$type][$name];
    }
    $cache_name = 'oopal-plugin-' . $type . '-' . $name;
    if ($cache = cache_get($cache_name)) {
      $plugin = $cache->data;
    }
    else {
      if (!$info) {
        if (isset($this->pluginInfo[$type]) && isset($this->pluginInfo[$type][$name])) {
          $info = $this->pluginInfo[$type][$name];
        }
      }
      if ($info) {
        $class = $info['class'];
        $plugin = new $class($info['args']);
        cache_set($cache_name, $plugin);
      }
    }
    $this->cache[$type][$name] = $plugin;
    return $plugin;
  }

  public function pluginsByType($type) {
    $plugins = array();
    if (isset($this->pluginInfo[$type])) {
      foreach ($this->pluginInfo[$type] as $name => $info) {
        $plugins[$name] = $this->load($type, $name, $info);
      }
    }
    return $plugins;
  }
}
