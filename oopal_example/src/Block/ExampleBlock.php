<?php

namespace Drupal\oopal_example\Block;

class ExampleBlock extends \Drupal\oopal\Block\BlockBase {
  public function adminLabel() {
    return t('oopal example: ExampleBlock');
  }
  public function label() {
    return t('ExampleBlock');
  }
  public function build() {
    return array(
      '#markup' => 'This is my example block.',
    );
  }
}
