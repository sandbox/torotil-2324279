<?php

/**
 * A list of plugin Interfaces keyed by their machine names.
 */
function hook_oopal_plugin_types_info() {
  $types['block'] = '\\Drupal\\oopal\\Plugins\\BlockInterface';
  return $types;
}

/**
 * Alter hook for @see hook_oopal_plugin_types_info().
 */
function hook_oopal_plugin_types_info_alter(&$types) {
  $types['block'] = '\\Drupal\\other_blocks\\Plugins\\BlockInterface';
}

/**
 * Declaration of plugin classes in an associative array.
 *
 * The array contains a list of plugins per plugin-type keyed by the
 * plugin-type's machine-name.
 *
 * Each item in the list is itself keyed by machine-name and contains the
 * following keys:
 *  - class: The name of the plugin-class. The class must implement the
 *           interface declared by the plugin-type.
 *  - args: (optional) an array of arguments for the plugin-class constructor.
 */
function hook_oopal_plugins_info() {
  $plugins['block']['example'] = array(
    'class' => '\\Drupal\\other_blocks\\Plugins\\FunnyBlock',
    'args' => array(),
  );
  return $plugins;
}

/**
 * Alter-hook for @see hook_oopal_plugins_info().
 */
function hook_oopal_plugins_info_alter() {
}
